# FastAPI 학습 일기 <sup>[1](#footnote_1)</sup>
몇 년 동안 Python 개발에서 손을 뗐다가 최근 OpenAI API를 사용해보고 싶어서 다시 시작하게 되었다. 지금은 Jupyter Notebook을 사용해 OpenAI API를 실험하는 중이다. 궁극적으로는 Django나 Flask의 백엔드에 몇 가지 멋진 기능을 추가할 생각이다.

Django를 마지막으로 사용한 지 약 5년이 지났고, Flask는 그보다 더 오래 사용했다. 요즘 둘 중 어느 쪽이 더 인기가 있는지 궁금해서 조사하던 중 새로운 것을 발견했다... 바로 FastAPI였다.

Django와 Flask는 모두 프론트엔드와 백엔드를 모두 처리하는 훌륭한 프레임워크이다. 저자는 프론트엔드 개발에 Vue.js를 사용하는 경우가 많아서 백엔드 기능에만 집중하는 FastAPI가 눈에 띄었다. 향후 프로젝트를 위한 완벽한 백엔드 프레임워크가 될 것 같아서 더 많은 것을 배우기 시작하게 되었다.

저자의 과거 경험에 비추어 볼 때, Python 웹 프레임워크로 개발할 때 가장 큰 걸림돌은 환경 설정이다. node.js에 비해 Python 버전과 배포판이 다양하고, 이를 모두 실행하기 위한 가상 환경 관리 도구도 무수히 많아서 항상 혼란스러웠다.

그런데 몇 년 전에 Django REST API에 관한 책을 읽고 Docker로 빌드하는 방법을 시도해 본 기억이 있다. 그때 사용했던 Dockerfile과 docker-compose 파일을 검색해봤다.

FastAPI에서도 똑같이 할 수 있으면 좋겠다는 생각이 들었고, 검색을 해보니 공식 문서에 Docker로 시작하는 방법에 대한 지침이 있었다.

- [개발용 Docker 컨테이너 만들기](./creating-a-docker-container-for-development.md)
- [도커 컨테이너에서 FastAPI 코드를 실행하기 위해 VSCode 디버거를 연결하는 방법](./connection-vscode.md)
- [요청 데이터 수신](./receiving-request-data.md)
- [Pydantic을 사용하여 요청 처리, 데이터베이스 저장, 응답 생성을 위한 모델 정의하기](./defining-models.md)
- [요청 헤더와 오류 응답 처리](./handling-request-headers-and-error-responses.md)
- [종속성 주입](./dependency-injection.md)


<a name="footnote_1">1</a>: 이 페이지는 다음을 편역한 것이다.

- [FastAPI Study Diary(1) — Creating a Docker Container for Development](https://medium.com/@mizutori/fastapi-study-diary-1-creating-a-docker-container-for-development-78a003cbd51f)
- [FastAPI Study Diary (2) — How to Attach VSCode Debugger to Running FastAPI Code on Docker Container](https://medium.com/@mizutori/fastapi-study-diary-2-how-to-attach-vscode-debugger-to-running-fastapi-code-on-docker-container-cc3c91fd9246)
- [FastAPI Study Diary (3) — Receiving Request Data](https://medium.com/@mizutori/fastapi-study-diary-3-receiving-request-data-394b5f914c90)
- [FastAPI Study Diary (4) — Defining Models for Request Handling, Database Storing, and Response Generation Using Pydantic](https://medium.com/@mizutori/fastapi-study-diary-4-defining-models-for-request-handling-database-storing-and-response-fe6bb30c0963)
- [FastAPI Study Diary (5) — Handling Request Headers and Error Responses](https://medium.com/@mizutori/fastapi-study-diary-5-handling-request-headers-and-error-responses-b9d23ed48747)
- [FastAPI Study Diary (6) — Dependency Injection](https://medium.com/@mizutori/fastapi-study-diary-6-dependency-injection-db6bd3b48a22)
