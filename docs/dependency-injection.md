# 종속성 주입
여기서는 FastAPI의 의존성 주입(DI)에 대해 살펴보겠다. 이전 포스팅([세 번째 포스팅](./receiving-request-data.md) 등)에서도 Pydantic의 BaseModel의 서브클래스로 요청을 받을 때 무의식적으로 DI를 사용했지만, 이번에는 특히 FastAPI에서 DI에 대해 더 깊이 파고들어가 보기로 한다. (이제부터는 종존성 주입을 간단히 DI라고 부를 것이다.)

FastAPI는 DI를 염두에 두고, 아니 오히려 DI를 용이하게 하기 위해 설계되었다.

## 함수에 의한 종속성 주입
주입을 위해 함수를 생성하고 이를 통해 종속성을 주입하는 방법이다.

아래와 같이 `pagination`이라는 이름의 비동기 함수를 아래와 같이 준비하였으며, 이 함수는 `offset`과 `limit`라는 두 개의 매개 변수를 받도록 설정되어 있다. 반환 값은 `offset`과 `limit`을 포함하는 튜플이다.

```python
async def pagination(offset: int = 0, limit: int = 10) -> Tuple[int, int]:
    return (offset, limit)

@app.get("/photos")
async def photo_list(query: Tuple[int, int] = Depends(pagination)):
    offset, limit = query
    return {"offset": offset, "limit": limit}
```

경로 연산 함수에서 `pagination` 함수를 `Depends` 함수에 전달하면 요청이 `pagination` 함수를 거치고 `offset`과 `limit` 값이 주입되는 메커니즘이 있다.

```bash
% http "http://127.0.0.1:8000/photos?offset=100&limit=5"
HTTP/1.1 200 OK
content-length: 24
content-type: application/json
date: Wed, 13 Sep 2023 09:46:34 GMT
server: uvicorn

{
    "limit": 5,
    "offset": 100
}
```

지난 글에서 이미 Pydantic 모델 클래스를 사용하여 요청 데이터를 수신하는 방법에 대해 기술하였지만, 이 방법만으로는 그다지 유리해 보이지 않을 수 있다,

```python
async def pagination(offset: int = 0, limit: int = 10) -> Tuple[int, int]:
    return (offset, max(10, limit))
```

이렇게 하면 `limit` 값이 10보다 작을 수 없고, 이 주입 함수 내에서 값의 유효성 검사와 변환을 자유롭게 수행할 수 있다는 점이 이 방법의 강점이다.

## 호출 가능한 클래스를 사용하는 종속성 주입
아래와 같이 호출 가능한 클래스를 사용하여 동일한 기능을 구현할 수 있다.

```python
class Pagination:
    def __init__(self, minimum_limit: int = 10):
        self.minimum_limit = minimum_limit

    async def __call__(
        self,
        offset: int = Query(0, ge=0),
        limit: int = Query(10, ge=0),
    ) -> Tuple[int, int]:
        validated_limit = max(self.minimum_limit, limit)
        return (offset, validated_limit)


pagination = Pagination(minimum_limit=50)
```

호출 가능한 클래스의 객체를 초기화한 후에는 아래와 같이 함수 기반 DI와 동일한 방식으로 사용할 수 있다.

```python
@app.get("/photos")
async def photo_list(query: Tuple[int, int] = Depends(pagination)):
    offset, limit = query
```

호출 가능한 클래스를 사용하면 값을 이니셜라이저(`__init__()`)에 동적으로 전달한 다음 종속성 주입 중에 활용할 수 있다는 이점이 있다. 이 예에서는 `Pagination` 클래스의 인스턴스를 생성할 때 `minimum_limit`이라는 값을 `__init__()`에 전달하여 객체 변수로 저장한 다음 종속성 주입 중에 사용한다. 이러한 유연성은 호출 가능한 클래스 사용의 강점 중 하나이다.

추가 조사 결과, 이 클래스를 사용한 종속성 주입은 호출 가능한 클래스 없이도 비슷한 기능을 달성할 수 있는 것으로 나타났다. 아래에서 볼 수 있듯이 일반 함수(이 경우 `get_offset_and_limit`)를 똑같은 방식으로 구현하여 `Pagination` 클래스의 인스턴스를 만들 수 있다.

```python
class Pagination:
    def __init__(self, minimum_limit: int = 10):
        self.minimum_limit = minimum_limit

    async def get_offset_and_limit(
        self,
        offset: int = Query(0, ge=0),
        limit: int = Query(10, ge=0),
    ) -> Tuple[int, int]:
        validated_limit = max(self.minimum_limit, limit)
        return (offset, validated_limit)


pagination = Pagination(minimum_limit=50)
```

경로 연산 함수를 구성할 때 `pagination` 객체를 Depends 함수에 전달하는 대신 아래와 같이 `pagination` 객체의 `get_offset_and_limit` 함수만 전달하면 된다.

```python
@app.get("/photos")
async def photo_list(query: Tuple[int, int] = Depends(pagination.get_offset_and_limit)):
    offset, limit = query
    return {"offset": offset, "limit": limit}
```

지금까지 구현해 보니 Python에서 함수는 'function pointer'라는 강력한 포지셔닝을 가지고 있다고 생각된다. C언어에서 자랐기 때문에 Python에 대해 다시 한 번 감사한다.

## 라우터를 통한 종속성 주입
또 다른 기능은 각 라우터에 대해 종속성 주입(DI)을 구성하는 기능으로, 여러 API에서 요청 헤더의 정보 유효성 검사같은 일반적인 미들웨어와 유사한 프로세스를 수행하려는 경우 특히 유용해 보인다.

먼저 아래와 같이 DI를 수신하는 호출 가능한 클래스를 설정한다. 이 `ApiTokenHeader` 클래스는 `__init__()`를 통해 API 토큰을 설정할 수 있다.

```python
from typing import Optional
from fastapi import FastAPI, Header, HTTPException, Depends, APIRouter

app = FastAPI()

class ApiTokenHeader:
    def __init__(self, api_token: str = "default_token"):
        self.api_token = api_token

    async def __call__(
        self,
        api_token: Optional[str] = Header(None),
    ) -> None:
        if not api_token or (api_token != self.api_token):
            print(api_token)
            raise HTTPException(status.HTTP_403_FORBIDDEN)

apiTokenHeader = ApiTokenHeader("some_secret_value")
```

DI 프로세스에서 요청 헤더에서 찾은 'api-token'이라는 헤더의 값을 api_token이라는 변수에 삽입한다. 헤더 이름에는 밑줄(`_`)을 포함할 수 없지만 FastAPI는 하이픈(`-`)을 밑줄로 자동 변환하므로 헤더 이름 'api-token'을 사용하여 요청을 보낼 수 있다. 이 `api_token`이 클래스 `__init__()`(self.api_token)에 설정된 것과 일치하면 프로세스가 정상적으로 완료되고 API의 작업(경로 연산 함수의 프로세스)이 계속된다. 일치하지 않으면 `403 Forbidden` 오류가 반환된다.

이 DI 호출 가능한 클래스를 적용하려면 `APIRouter` 객체를 생성할 때 `Depends()` 함수에 전달한 다음 아래와 같이 종속성 목록을 설정한다.

```python
router = APIRouter(dependencies=[Depends(apiTokenHeader)])

@router.get("/account")
async def get_account():
    return {"account": "user account"}


@router.get("/account_detail")
async def get_account_detail():
    return {"account": "user account detail info"}


app.include_router(router, prefix="/user")
```

이 설정에 따라 이후 **라우터에 추가되는 모든 API에 DI 호출 가능 클래스가 적용된다**. 즉, 이제 모든 API가 헤더의 `api-token`에 대한 검사를 수행한다. 이렇게 하면 각 API의 헤더에 대한 토큰 유효성 검사를 개별적으로 작성할 필요가 없으므로 매우 편리하다. 아래 그림과 같이 'api-token'이 올바르게 설정되면 응답이 성공적으로 반환된다.

```python
% http GET http://127.0.0.1:8000/user/account  'api-token: some_secret_value'
HTTP/1.1 200 OK
content-length: 26
content-type: application/json
date: Wed, 13 Sep 2023 10:20:48 GMT
server: uvicorn

{
    "account": "user account"
}

% http GET http://127.0.0.1:8000/user/account_detail  'api-token: some_secret_value'
HTTP/1.1 200 OK
content-length: 38
content-type: application/json
date: Wed, 13 Sep 2023 10:21:19 GMT
server: uvicorn

{
    "account": "user account detail info"
}
```

(명확성을 위해 이번에는 간단한 헤더 토큰 검사를 구현했다. 실제 API를 보호하기에 충분한 보안을 보장하지는 않는다는 점에 유의하세요.)

이렇게 각 라우터에 DI 메커니즘을 적용하면 아래와 같이 헤더 검사가 필요 없는 오픈 API를 별도의 APIRouter 객체에 추가하여 보안 수준을 차별화할 수 있다.

```python
router_public = APIRouter()

@router_public.get("/advertisement")
async def get_ad():
    return {"advertisement": "some advertisement"}


@router_public.get("/contact")
async def get_contact():
    return {"contact": "contact info"}


app.include_router(router_public, prefix="/public")
```

이 경우 `/public`은 공개 API용이고 `/user`는 헤더 인증이 필요한 경우로 쉽게 구분할 수 있다.

아래 예처럼 헤더를 포함하지 않고도 `/public`에서 API에 액세스하여 응답을 받을 수 있다.

```python
% http GET http://127.0.0.1:8000/public/contact
HTTP/1.1 200 OK
content-length: 26
content-type: application/json
date: Wed, 13 Sep 2023 10:32:28 GMT
server: uvicorn

{
    "contact": "contact info"
}
```

반면, 헤더 정보 없이 `/user`로 API에 액세스하거나 잘못된 토큰을 보내면 `HTTP Status = 403 Forbidden`이 반환된다.


```bash
% http GET http://127.0.0.1:8000/user/account_detail
HTTP/1.1 403 Forbidden
content-length: 22
content-type: application/json
date: Wed, 13 Sep 2023 10:34:45 GMT
server: uvicorn

{
    "detail": "Forbidden"
}


% http GET http://127.0.0.1:8000/user/account  'api-token: i-dont-know'
HTTP/1.1 403 Forbidden
content-length: 22
content-type: application/json
date: Wed, 13 Sep 2023 10:38:47 GMT
server: uvicorn

{
    "detail": "Forbidden"
}
```

이 과정을 통해 FastAPI의 의존성 주입(DI)을 사용하여 제품에 필요한 API를 구성하는 방법에 대해 확실하게 이해하게 되었다.

사실 "의존성 주입"이라는 용어에 대해 다소 호의적이지 않다. 안드로이드 앱에 의존성 주입 프레임워크나 Dagger와 같은 표기법을 도입하면 코드를 읽기 어렵게 만들고 프로그래밍의 자유도를 떨어뜨릴 수 있다고 생각했기 때문이다. 또한 일정 수준 이상의 팀에서 개발하지 않는다면 버그의 온상이 되어 디버깅이 악몽이 될 수도 있다. 저자는 Dagger로 엄격하게 작성된 앱을 복구하는 작업을 맡은 적이 있는데, 그때는 각 종속성을 힘들게 풀고 혼란스러운 메모리 할당을 바로잡아야 했다.

이번에 FastAPI로 DI를 구현하면서, 종속성성 주입이 세션의 시작과 끝이 명확하게 정의되어 있는 REST API에 매우 편리하다는 것을 알았다. 또한 DI가 Java 백엔드 프레임워크 커뮤니티에서 처음 인기를 얻은 이유도 이해할 수 있었다.

반대로, 사용자 상호 작용이 방대하고 앱이나 화면의 수명 주기가 매우 동적이고 예측할 수 없는 설치형 모바일 앱을 개발하는 데 종속성 주입을 사용하는 것이 과연 최적의 솔루션인지 생각해 볼 수 있었다. 수많은 인터랙션과 라이프사이클 패턴 때문에 DI를 통해 중앙에서 관리하려는 동기가 발생한다고 생각하지만, 많은 사람들이 중앙에서 관리되는 컴포넌트가 어떻게 사용될지에 대한 완벽한 이미지를 가지고 DI를 사용할 수 있을지 다시 생각해보게 되었다.
