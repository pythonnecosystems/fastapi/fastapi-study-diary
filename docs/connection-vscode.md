# 도커 컨테이너에서 FastAPI 코드를 실행하기 위해 VSCode 디버거를 연결하는 방법

[이전 포스팅](./creating-a-docker-container-for-development.md)에서는 로컬 컴퓨터의 코드 변경 사항이 컨테이너에 즉시 반영되고 브라우저에서 확인할 수 있는 Docker 컨테이너에서 실행되는 FastAPI를 사용하여 개발 환경을 설정했다.

이제 디버거를 사용하여 컨테이너 내부의 코드를 일시 중지하고 단계별로 살펴볼 수 있다면 로컬 머신에서 직접 FastAPI를 실행하여 개발하는 것보다 열등하지 않은 환경을 갖게 될 것이다. 컨테이너 내부의 FastAPI 코드에 대해 VSCode 디버거를 실행하는 실험을 해보자.

## Dockerfile
다음과 같이 Dockefile을 구성한다.

```
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7
# Note: In default, the working directory is /app in this base image.

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY ./app /app
CMD ["python3", "-m", "debugpy", "--listen", "0.0.0.0:5678", "-m", "uvicorn", "main:app", "--reload", "--host", "0.0.0.0", "--port", "8000"]
```

지난번과 다른 점은 마지막 명령어뿐이다. 이전에는 이렇게 uvicorn을 실행하였다.

```
CMD ["uvicorn", "main:app", "--reload", "--host", "0.0.0.0", "--port", "3000"]
```

그리고 아래와 같이 변경한다.

```
CMD ["python3", "-m", "debugpy", "--listen", "0.0.0.0:5678", "-m", "uvicorn", "main:app", "--reload", "--host", "0.0.0.0", "--port", "8000"]
```

`debugpy`를 실행한 다음 `debugpy`를 통해 `uvicorn`을 실행한다. 마치 debugpy를 통해 uvicorn을 실행하는 것 같다(나중에 debugpy에 대해 더 자세히 알아보도록 하겠다).

## requirements.txt
`debugpy` 모듈을 사용하려면 `requirements.txt`에 다음 줄을 추가하세요.

```
debugpy
```

## docker-compose.yml
지난번과 다른 점은 컨테이너의 5678 포트를 호스트 머신의 5678 포트로 열었다는 점이다.

```yml
version: '3.7'

services:
  api:
    build: .
    container_name: "api"
    volumes:
      - ./app:/app
    ports:
    - 8000:8000
    - 5678:5678
```

컨테이너 내부에서 debugpy는 포트 5678에서 연결을 수신 대기한다. 이렇게 하면 VSCode 디버거가 중단점에서 코드를 중단하거나 실행 중에 코드를 단계별로 살펴볼 수 있다.

## launch.json
VSCode를 열고 'Run and Debug' 아이콘(아이콘이 재생 버튼처럼 보인다)을 클릭한다.

![](./images/1_sfXqPd_wN_WJYQoWX5ygqg.webp)

화면 상단의 ‘create a launch.json file’ 링크에서 `launch.json` 파일을 생성할 수 있다.

`launch.json`의 내용을 다음과 같이 설정한다.

```json
{  
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Python: FastAPI Container Debug",
      "type": "python",
      "request": "attach",
      "port": 5678,
      "host": "localhost",
      "pathMappings": [
          {
              "localRoot": "${workspaceFolder}/app",
              "remoteRoot": "/app"
          }
      ]
    }
  ]
}
```

까다로운 부분은 `localRoot`와 `remoteRoot`이다. `localRoot`의 경우, 로컬 컴퓨터에서 `main.py`가 있는 폴더를 지정한다. `remoteRoot`의 경우 해당 폴더를 복사한 Docker 컨테이너 내부의 폴더를 지정한다. Dockerfile에 다음 명령을 작성하여 로컬 머신에서 컨테이너로 폴더를 복사했다. 동일한 위치를 지정하는 한 문제가 없을 것이다.

```
COPY ./app /app
```

또 다른 중요한 점은 포트 5678이 지정되어 있다는 것이다.

```
"port": 5678,
```

이 설정을 사용하면 이 `launch.json`을 사용하여 디버거를 시작하면 포트 5678을 통해 컨테이너 내부의 `debugpy`와 통신한다.

## 컨테이너 시작
다음 `docker-compose` 명령을 사용하여 Docker 이미지를 빌드하고 컨테이너를 시작한다.

```bash
$ docker-compose up --build
```

(두 번째부터는 `Dockerfile` 또는 `docker-compose.yml`을 편집하지 않은 경우 `--build` 옵션이 필요하지 않다.)
먼저 브라우저에서 localhost:8000에 접속하여 컨테이너와 그 안의 FastAPI가 실행 중인지 확인한다.

![](./images/1_X-OPm3bw62pgGtzuQkCulw.webp)

## Breakpoint 설정과 디버거 시작하기
VSCode를 열고 `app/main.py`로 이동한 다음 `"/"`(루트) API 처리기에 breakpoint를 설정한다.

![](./images/1_WAZ9E0ZMljRp1FP4g4gQ6Q.webp)

그런 다음 VSCode에서 `Run and Debug` 메뉴를 열고 `launch.json`에서 방금 만든 구성을 선택한 다음 디버거를 시작한다.

![](./images/1_LjvSOEaknJQIdH-KxHt2lw.webp)

브라우저에서 'localhost:8000/'를 연다. `"/"`(루트) API를 누르면 다음과 같이 프로그램이 breakpoint에서 일시 정지한다!

![](./images/1_G6Eg7K9hLbe52gilW8nqeg.webp)

이제 로컬 머신을 엉망으로 만들지 않고도 컨테이너 내에서 원하는 환경을 만들고, 코드를 편집하고, 디버거를 사용하여 디버깅할 수 있다. 우리에게 이상적인 개발 환경될 수 있다.

수고하셨습니다. 커피 한 잔 할 시간이네요. 다음에는 요청 데이터 유효성 검사에 대해 알아보거나 어떤 ORM 매퍼가 가장 적합한지 알아보도록 하겠다.

계속 지켜봐 주세요.
