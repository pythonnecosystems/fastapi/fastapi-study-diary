# 개발용 Docker 컨테이너 만들기
"정말 대단한 시대입니다... " 이를 참고하여 과거에 만든 구성과 혼합하여 환경을 설정하기로 결정했다.

디렉토리 구조를 이렇게 구성하였다. 공식 문서에는 Dockerfile만 언급되어 있지만, 시작 명령을 간소화하기 위해 `docker-compose.yml`도 추가했다.

![](./images/1_T-MH6K966Sl2X4gWpbl7fQ.webp)

```
.
├── app
│   └── main.py
├── docker-compose.yml
├── Dockerfile
└── requirements.txt
```

FastAPI는 공식적으로 Docker 이미지를 제공하므로 이를 빌린 다음 컨테이너의 `/app`에 `app` 디렉토리를 복사했다. 마지막으로 `uvicorn`을 사용하여 앱을 시작했다.

다음은 최종 Docker 파일이다.

```yml
FROM tiangolo/uvicorn-gunicorn:python3.11

LABEL maintainer="Sebastian Ramirez <tiangolo@gmail.com>"

COPY requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt

COPY ./app /app

CMD ["uvicorn", "main:app", "--reload", "--host", "0.0.0.0", "--port", "8000"]
```

FastAPI에서 일반적으로 사용되는 포트 8000을 모니터링하도록 설정했다.

## docker-compose.yml

다음으로 `docker-compose.yml`을 작성한다. 이것은 현재 디렉토리에서 이미지를 빌드한 다음 이를 사용하여 `api`라는 이름의 컨테이너를 시작한다.

```yml
version: '3.11'

services:
  api:
    build: .
    container_name: "api"
    volumes:
      - ./app:/app
    ports:
    - 8000:8000
```

`volumes:` 섹션에서 컨테이너의 `/app` 디렉터리가 로컬 `app` 디렉터리에 바인딩되도록 구성했다. 또한 컨테이너의 포트 8000을 호스트 머신의 포트 8000에 연결했다.

## requirements.txt

```
uvicorn[standard]==0.20.0
gunicorn==20.1.0
fastapi[all]==0.88.0
```

## main.py
`main.py`는 공식 코드를 그대로 사용했다. 아직 FastAPI를 배우기 시작하지 않았기 때문에 FastAPI의 코딩에 대해서는 전혀 모른다. 하지만 node.js와 비슷하고 `"/"`와 `"/items"`이라는 두 가지 샘플 API가 구현되어 있다는 것을 알 수 있다.

```python
from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}
```

이제 **`docker compose up`**을 사용하여 컨테이너를 시작해 보겠다.

브라우저에서 http://localhost:8000/ 에 접속하면 아래와 같이 API가 실행되고 있음을 확인할 수 있다.

![](./images/1_7PN5lnDjZveoDOM-Sik2dg.webp)

앞서 설명했듯이, `docker-compose.yml`에서 로컬 컴퓨터의 `app` 디렉토리는 컨테이너의 `/app` 디렉토리에 바인딩된다.

```
    volumes:
      - ./app:/app
```

`main.py`에서 루트 API( `"/"` )의 응답을 `"Hello"'`에서 `"Good Morning"`으로 변경하면 다음과 같이 된다.

```python
@app.get("/")
def read_root():
    return {"Good Morning": "World"}
```

API의 응답이 즉시 변경되는 것을 확인할 수 있다. (변경 사항을 확인하려면 페이지를 새로 고침하고 API에 새 가져오기 요청을 보내야 한다.)

![](./images/1_X-OPm3bw62pgGtzuQkCulw.webp)

`docker-compose.yml`의 `volumes` 태그를 사용하여 호스트와 컨테이너 디렉터리를 바인딩하면 컨테이너를 다시 시작하지 않고도 코드 변경 사항이 반영되므로 개발 중에 매우 편리하다.

이 시리즈에서 더 많은 내용을 기대해주세요...
