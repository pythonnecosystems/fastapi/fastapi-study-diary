# 요청 데이터 수신
이제 FastAPI 개발 환경이 설정되었으므로 FastAPI에서 요청을 처리하는 방법에 대해 알아보겠다.

## 경로 매개변수 수신

```python
@app.get("/books/{book_id}")
def get_book(book_id: int):
    return {"book_id": book_id}
```

Node와 비슷해서 개념을 쉽게 파악할 수 있었다. 한 가지 주의할 점은 Python에도 타입 사양이 있다는 것이다. 위의 예에서 함수는 `book_id`를 `int` 타입으로 받아들이도록 작성되어 있다. (이 요청 처리 메커니즘이 타입 사양을 위해 Pydantic이라는 것을 사용한다.)

방금 만든 API를 실행해보기로 한다.

브라우저에서 로컬 호스트의 8000 포트에 있는 `/books/15` 경로에 액세스했을 때, 응답 JSON에서 `15` 부분이 `book_id`에 int 타입 데이터로 입력된 것을 확인할 수 있다.

![](./images/1_50uQAzQ2yaoQoM9IW3KuJQ.webp)

다음으로 다음과 같이 `/15` 대신 `/great_gatsby` 경로에 액세스해 보겠다.

![](./images/screenshot_01.png)

아래와 같은 응답이 반환된다.

```
{"detail":[{"type":"int_parsing","loc":["path","book_id"],"msg":"Input should be a valid integer, unable to parse string as an integer","input":"great_gatsby","url":"https://errors.pydantic.dev/2.5/v/int_parsing"}]}
```

`great_gatsby` 데이터를 정수로 변환할 수 없어 오류가 발생한 것 같다.

실행 중 FastAPI 로그에서 아래와 같은 오류가 발생했음을 확인할 수 있다.

```
“GET /books/great_gatsby HTTP/1.1” 422 Unprocessable Entity
```

## 중첩된 경로 매개변수 검색
이전에 이어서 아래와 같이 요청에 여러 경로 매개변수를 수신해 보겠다.

```python
@app.get("/books/{book_type}/{book_id}")
def get_book(book_type: str, book_id: int):
    return {"book_type": book_type, "book_id": book_id}
```

http://127.0.0.1:8000/books/english/3 을 액세스하면 다음과 같은 응답이 표시된다.

```
{"book_type": "english", "book_id": 3}
```

이는 Node.js의 작동 방식과 유사하므로 이해하기 쉽다.

## 유효성 검사
FastAPI를 통해 요청 데이터에 대한 유효성 검사를 얼마나 쉽게 추가할 수 있는지 보였다.

```python
@app.get("/books/{book_id}")
def get_book(book_id: int = Path(..., ge=100)):
    return {"book_id": book_id}
```

예를 들어 타입을 `int`로 지정한 이전 예에서 `= Path(..., ge=100)`를 추가하면 아래와 같이 `book_id` 값이 100보다 작을 때 오류 422가 반환된다.

![](./images/1_Lo7tQ86dYvWY8C9aKkUVRA.webp)

`Path(..., ge=100)`의 `...`는 일반적으로 기본값을 지정하는 위치이지만 이 경우 기본값이 없으므로 `...`가 대신 사용되었다.

`ge=100`의 `ge`는 "보다 크거나 같은"의 약어로, 이 경우 "100 이상"을 나타낸다. 즉, `Path(..., ge=100)`는 `book_id`가 `100` 이상이어야 한다는 제약 조건을 추가한다.

다음과 같이 다른 옵션을 사용할 수 있다.
- `gt`: "보다 큰"
- `lt`: "미만"
- `le`: "다음보다 작거나 같음"

이를 통해 각각의 조건을 지정할 수 있다.

## 문자열 유효성 검사

```python
@app.get("/search_book/{book_name}")
def search_book(book_name: str = Path(..., min_length=2, max_length=20)):
    return {"book_name": book_name}
```

위와 같이 `book_name`에 단일 문자 문자열을 지정하면 422 오류가 발생한다.

![](./images/screenshot_02.png)

이러한 방식으로 최소 또는 최대 글자 수를 추가할 수 있다.

## 쿼리 매개변수
경로 매개변수 대신 쿼리 매개변수는 경로 정의(`@app.get()`)로 정의되지 않고 함수 매개변수에만 포함된다.

```python
@app.get("/search_book/{book_name}")
def search_book(book_name: str, offset: int = 0, limit: int = 10):
    return {"book_name": book_name, "offset": offset, "limit": limit}
```

매우 간단하고 이해하기 쉽다고 생각할 수 있다.

쿼리 매개변수를 설정하지 않고 요청을 보내는 경우, 다음과 같다.

```
http://127.0.0.1:8000/search_book/great
```

![](./images/1_9ChQPixx5to8u5VNrQAbFA.webp)

브라우저 스크린샷에서 볼 수 있듯이 함수 파라미터에 설정된 기본값(0과 10)이 `offset`과 `limit` 변수에 자동으로 할당된다.

아래 URL에서와 같이 쿼리 매개변수를 설정할 수 있다,

```
http://127.0.0.1:8000/search_book/great?offset=100&limit=50
```

![](./images/screenshot_03.png)

쿼리 매개변수에 지정된 값은 그에 따라 할당된다.

쿼리 매개변수에 유효성 검사를 추가하려면 아래와 같이 Query 클래스를 사용하면 된다.

```python
from fastapi import FastAPI, Path, Query

@app.get("/search_book/{book_name}")
def search_book(book_name: str, offset: int = Query(0, ge=0), limit: int = Query(10, le=100)):
    return {"book_name": book_name, "offset": offset, "limit": limit}
```

Query 클래스의 생성자에 전달되는 첫 번째 값은 기본값이고, 두 번째 값은 유효성 검사 조건을 위한 값이다.

`http://127.0.0.1:8000/search_book/great?offset=100&limit=500` 같은 요청을 보내면 아래와 같이 422 오류가 반환된다.

![](./images/screenshot_04.png)

## Post 요청
아래와 같이 `Body` 클래스를 사용하여 POST 요청 본문에서 정보를 받을 수 있다.

```python
@app.post("/photos")
async def create_photo(title: str = Body(...), filename: str = Body(...), price: int = Body(...)):
    return {"tilte": title, "filename": filename, "price": price}
```

한 가지 중요한 점은 `@app.get()`에서 `@app.post()`로 변경해야 한다는 것이다.

Postman은 이 POST API를 테스트하기에 좋은 도구이지만, 이번에는 HTTPie라는 도구를 사용해 보기로 하자.

## HTTPie
brew를 사용하여 HTTPie를 설치한다.

```bash
$ brew install httpie
```

HTTPie를 사용하면 JSON을 매우 쉽게 게시할 수 있다. 엔드포인트 뒤에 key와 value 쌍을 입력하면 이러한 key=value 쌍이 포스트 요청에 JSON 데이터로 포함된다.

```
% http POST http://127.0.0.1:8000/photos title="cat" filename="cat1.jpg" price="500"
HTTP/1.1 200 OK
content-length: 49
content-type: application/json
date: Sun, 24 Dec 2023 11:22:47 GMT
server: uvicorn

{
    "filename": "cat1.jpg",
    "price": 500,
    "tilte": "cat"
}
```

API는 게시된 데이터를 그대로 반환하기 때문에 요청이 제대로 수신되었는지 확인할 수 있다.

참고로 `-v` 옵션을 추가하면 요청 내용도 로그에 나타나기 때문에 디버깅 시 헤더와 요청 본문의 내용을 확인할 수 있다.

```bash
% http -v POST http://127.0.0.1:8000/photos title="cat" filename="cat1.jpg" price="500"
POST /photos HTTP/1.1
Accept: application/json, */*;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 56
Content-Type: application/json
Host: 127.0.0.1:8000
User-Agent: HTTPie/3.2.2

{
    "filename": "cat1.jpg",
    "price": "500",
    "title": "cat"
}


HTTP/1.1 200 OK
content-length: 49
content-type: application/json
date: Fri, 08 Sep 2023 07:45:55 GMT
server: uvicorn

{
    "filename": "cat1.jpg",
    "price": 500,
    "title": "cat"
}
```

## Pydantic 모델
게시할 데이터가 커지면 이전 예에서처럼 모든 데이터를 파라미터로 정의하는 것이 번거로워지고 가시성도 떨어진다.

Pydantic 모델을 사용하면 요청 내용을 객체로 생성할 수 있으므로 매우 편리하다.

```python
# Newly added import
from pydantic import BaseModel

...



class Photo(BaseModel):
    title: str
    filename: str
    price: int


@app.post("/photos")
async def create_photo(photo: Photo):
    return photo
```

위의 예는 이전 예와 동일한 JSON을 수신하는 API이지만 이번에는 요청에서 데이터를 Photo 클래스의 객체로 받는다. 수신한 Photo 클래스 객체(photo)는 클라이언트에게 그대로 반환되지만, 자동으로 JSON으로 변환되어 편리하다.

이렇게 요청을 객체로 받으려면 Photo 클래스가 Pydantic의 BaseModel을 상속하는 클래스여야 한다. 이는 매우 중요한 포인트이다.

원하는 클래스를 BaseModel의 서브클래스로 정의하면 Pydantic이 데이터 변환, 유효성 검사와 변수 할당을 대신 처리한다.

Pydantic을 처음 배웠는데, 이를 사용하면 요청 데이터에 대한 보다 광범위한 유효성 검사(예: 요청에 사진 객체에 필요한 데이터가 포함되어 있는지 확인)가 가능해지므로 FastAPI 구현의 중심이 될 것으로 보인다.

다음 [포스팅](./defining-models.md)에서 Pydantic에 대해 더 자세히 알아보고 더 많은 인사이트를 공유할 예정이다.
