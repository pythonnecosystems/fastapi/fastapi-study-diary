# 요청 헤더와 오류 응답 처리
지난 두 포스팅에서 GET 요청의 파라미터와 POST 요청의 본문을 다루면서 FastAPI에서 요청 데이터를 수신하는 방법을 보였다. 이번에는 헤더를 처리하는 방법과 같은 다른 측면을 살펴보고자 한다.

## 헤더 검색
아래와 같이 헤더 클래스를 사용하여 헤더에 저장된 정보를 수신할 수 있다.

```python
@app.get("/")
def read_header(user_agent: str = Header(...)):
    return {"user_agent": user_agent}
```

놀랍게도 위의 코드는 헤더에서 `User-Agent` 값을 자동으로 검색하여 `user_agent` 변수에 할당한다. 그 뒤에는 헤더의 `User-Agent`를 모두 소문자 `user_agent`로 변환한 다음 Python의 변수 명명 규칙에 따라 `-`을 `_`로 변환하여 경로 연산 함수가 `user_agent`와 같은 이름의 매개변수를 받을 수 있도록 한다.

![](./images/1_DTH3r32OAs4hMh_YuCx69A.webp)

이 변환은 User-Agent와 같은 필수 헤더 키뿐만 아니라 클라이언트 측 프로그램에서 추가할 수 있는 커스텀 key=value 쌍에도 적용된다. 이러한 경우에도 변환 규칙은 동일하다.

예를 들어 아래와 같이 헤더에서 `hello_beautiful_world` 변수로 값을 받도록 설정한 경우이다,

```python
@app.get("/header")
def read_header(hello_beautiful_world: str = Header(...)):
    return {"hello_beautiful_world": hello_beautiful_world}
```

헤더에 `Hello-Beautiful-World` 키와 함께 값이 전달되면 이 값은 `hello_beautiful_world`에 입력된다.

```bash
% http GET http://127.0.0.1:8000/header 'Hello-Beautiful-World: Hello'
HTTP/1.1 200 OK
content-length: 33
content-type: application/json
date: Mon, 11 Sep 2023 09:09:10 GMT
server: uvicorn

{
    "hello_beautiful_world": "Hello"
}
```

대문자는 모두 소문자로 변환되기 때문에 아래와 같이 'hEllO-beautiful-worlD'를 보내도 같은 방식으로 작동하지 않을까 걱정했는데, 문제 없이 변환되었다.

```bash
% http GET http://127.0.0.1:8000/header 'hEllO-beautiful-worlD: Hello'
HTTP/1.1 200 OK
content-length: 23
content-type: application/json
date: Mon, 11 Sep 2023 09:10:11 GMT
server: uvicorn

{
    "hello_beautiful_world": "Hello"
}
```

## 상태 코드(Status Code)
FastAPI는 기본적으로 Status Code 200을 반환한다. 이 상태 코드를 변경하려면 경로 연산 함수에서 응답 객체를 수신하고 아래와 같이 status_code 값을 수정하면 된다.

```python
@app.post("/photos")
async def create_photo(photo: Photo, response: Response):
    response.status_code = 201
    return photo
```

(미리 fastapi 모듈에서 Response 클래스를 가져와야 한다).

```bash
% http POST http://127.0.0.1:8000/photos title="cat2" filename="cat2.jpg" price="500"
HTTP/1.1 201 Created
content-length: 50
content-type: application/json
date: Mon, 11 Sep 2023 09:48:14 GMT
server: uvicorn

{
    "filename": "cat2.jpg",
    "price": 500,
    "title": "cat2"
}
```

성공적인 작업에 대한 기본 상태 코드를 경로 정의의 매개변수로 설정할 수도 있다(FastAPI에서는 데코레이터라고 한다).

```python
@app.post("/photos", status_code=status.HTTP_201_CREATED)
async def create_photo(photo: Photo):
    return photo
```

(반드시 fastapi 모듈에서 상태 클래스를 가져와야 한다.)

전자의 방법은 프로그램의 프로세스 내용에 따라 상태 코드를 동적으로 변경하려는 경우에 사용되는 반면, 후자의 방법은 기본 상태 코드를 변경하려는 의도가 있을 때 사용되는 것으로 보인다.

## 오류 던지기
마지막으로 오류(오류 상태 코드)를 반환하려는 경우에 대한 응답을 구현하는 방법은 다음과 같다.

```python
@app.get("/secret/{secret_id}")
def erro(secret_id: str):
    if secret_id != "ok":
        raise HTTPException(status_code=403, detail="Access Forbidden")
    return {'message': 'id is ok'}
```

(fastapi 모듈에서 HTTPException 클래스를 가져와야 한다).

문제가 발생했을 때 `HTTPException` 클래스의 객체를 생성하고 이를 발생시키면 FastAPI는 자동으로 이 예외를 처리하고 `HTTPException` 객체에 설정된 상태 오류 코드가 포함된 응답을 클라이언트에 반환한다.

```bash
% http GET http://127.0.0.1:8000/secret/no
HTTP/1.1 403 Forbidden
content-length: 29
content-type: application/json
date: Mon, 11 Sep 2023 10:01:29 GMT
server: uvicorn

{
    "detail": "Access Forbidden"
}
```

위의 실제 응답에서 볼 수 있듯이 `HTTPException` 객체의 세부 정보에 지정된 문자열이 응답 본문의 JSON 데이터에 'detail' 키의 값으로 삽입된다.

참고로 Python에서 `raise`는 다른 프로그래밍 언어에서 `throw`와 동일하다.

이 시리즈에서 더 많은 내용을 기대해주세요...
