# Pydantic을 사용하여 요청 처리, 데이터베이스 저장, 응답 생성을 위한 모델 정의하기

[FastAPI 학습 일기의 세 번째 편](./receiving-request-data.md)에서는 요청에서 데이터를 처리하는 방법을 배웠다.

이 포스팅에서는 Pydantic의 모델로 요청 데이터를 수신하는 방법에 대해 자세히 알아보겠다.

FastAPI를 사용하면 Post 요청으로 받은 JSON 데이터를 Pydantic 모델로 직접 변환하여 경로 연산 함수의 인수로 받을 수 있다.

이를 수행하는 방법을 살펴보겠다.

Pydantic의 BaseModel을 사용하려면 다음 import 문을 추가한다.

```python
from pydantic import BaseModel
```

먼저 이 BaseModel의 서브클래스를 생성한다. 이름을 `PhotoBase`로 지정한다.

```python
class PhotoBase(BaseModel):
    title: str
    filename: str
    price: int
```

현재 이 클래스는 API에서 요청을 수신하는 '수신자' 객체 클래스가 됩니다. 그러나 REST API를 구현할 때 요청에 들어오는 요소는 POST(CREATE), PUT(UPDATE) 또는 PATCH(UPDATE) 작업인지에 따라 달라진다. 따라서 API의 목적에 따라 클래스를 생성하는 것이 FastAPI의 방식인 것 같다.

이번에는 POST API를 만들고 싶기 때문에 PhotoCreate라는 클래스를 BaseModel의 서브클래스가 아닌 PhotoBase의 서브클래스로 정의하겠다.

```python
class PhotoCreate(PhotoBase):
    pass
```

POST API를 만들 때 PhotoBase 클래스의 모든 요소를 사용한다. 추가할 요소가 없으므로 그냥 `pass`를 넣는다.

다음으로 데이터베이스에 사진 데이터를 추가하려면 데이터베이스에 있는 사진 테이블의 요소를 고려한 클래스를 만들어야 한다. 예를 들어, `id`와 `number_of_downloads`(다운로드 횟수)처럼 API 요청에는 포함되지 않지만 데이터가 데이터베이스에 삽입될 때 초기화되거나 삽입 중에 프로그램에 의해 자동으로 채워지는 멤버 변수가 있는 클래스를 정의한다.

```python
class PhotoDB(PhotoBase):
    id: int
    number_of_downloads: int = 0
```

마지막으로 응답 데이터에 대한 클래스를 준비해야 한다. 응답에는 PhotoBase의 멤버 변수를 포함하고 싶지만, 클라이언트에게 반환할 `id`와 같이 데이터베이스에 삽입할 때 할당된 데이터도 포함해야 하는 경우가 있다. 이러한 경우 아래와 같이 아이디만 추가하는 클래스를 생성한다.

```python
class PhotoPublic(PhotoBase):
    id: int
```

이러한 클래스를 사용하여 API를 구현하면 다음과 같다.

```python
@app.post("/photos", status_code=status.HTTP_201_CREATED, response_model=PhotoPublic)
async def create(photo_create: PhotoCreate):
    post = PhotoDB(id=3, **photo_create.dict())
    return post
```

경로 정의에서 응답 `response_model=PhotoPublic`을 추가하면 응답 타입이 자동으로 `PhotoPublic`으로 변환된다. 경로 연산 함수(`create()`)에서 `PhotoCreate` 클래스의 객체로부터 요청의 JSON 데이터를 수신한다.

 ```python
async def create(photo_create: PhotoCreate):
 ```

다음 섹션에서는 데이터베이스에 삽입할 데이터로 `PhotoDB` 객체를 생성한다.

```python
post = PhotoDB(id=3, **photo_create.dict())
```

(이 예에서는 실제 데이터베이스를 사용하지 않으므로 임의의 값으로 `id`를 초기화한다. 실제로는 `photo_create()` 값을 데이터베이스의 ORM 매퍼 함수에 전달하고 그 결과로 데이터베이스가 자동으로 할당하는 `id`를 PhotoDB 클래스의 객체로 받을 수 있다).

그런 다음 이 `PhotoDB` 객체를 반환한다. 그러나 이전에 경로 정의에 `response_model=Photopublic`을 포함했기 때문에 응답의 JSON 데이터에는 `PhotoPublic`의 요소, 즉 `id`, `title`, `filename`과 `price`가 포함된다.

다음은 이 API에 POST 요청을 보낸 실제 결과이다.

```bash
% http POST http://127.0.0.1:8000/photos title="cat2" filename="cat2.jpg" price="500"
HTTP/1.1 201 Created
content-length: 57
content-type: application/json
date: Wed, 13 Sep 2023 05:44:35 GMT
server: uvicorn

{
    "filename": "cat2.jpg",
    "id": 3,
    "price": 500,
    "title": "cat2"
}
```

오랜만에 Python으로 돌아와서 아래 코드에서 PhotoDB 객체를 생성하는 데 사용된 표기법, 즉 `**` 연산자를 잊어버렸다.

```python
post = PhotoDB(id=3, **photo_create.dict())
```

`photo_create.dict()`의 의미는 `PhotoCreate` 객체인 `photo_create`의 내용을 dict 타입(JSON과 유사)으로 변환한 다음 key=value 형태로 추출하는 것이다.

`photo_create.dict()`는 아래와 같은 데이터를 응답한다.

```python
{
    filename: "cat2.jpg",   
    price: 500,
    title: "cat2"
}
```

다음이 키=값 쌍으로 변환된 것이다.

```python
filename="cat2.jpg", price=500, title="cat2"
```

이전 코드

```python
post = PhotoDB(id=3, **photo_create.dict())
```

는 아래 코드와 같은 동일하다.

```python
post = PhotoDB(id=3, filename="cat2.jpg", price=500, title="cat2")
```

따라서 PhotoDB 객체의 인스턴스를 생성하였다.

다른 프로그래밍 언어에 익숙한 개발자는 코드를 읽을 때 흐름에 맞춰서 읽다 보면 이 구문이 직관적이지 않을 수 있다.

이 패턴처럼 목적별로 모델 클래스를 정의하여 프로그램을 작성하는 경우에는 이 `**`를 자주 사용될 것 같다.

## PATCH 메소드로 부분적으로 업데이트하기
아래와 같이 업데이트 클래스를 만들고 필드를 선택 사항으로 정의한다.

```python
class PhotoUpdate(PhotoBase):
    title: Optional[str] = None
    filename: Optional[str] = None
    filename: Optional[int] = None
```

`PhotoUpdate` 클래스로 요청을 받으면 요청에 포함된 필드에는 값이 있고 요청에 포함되지 않은 필드는 None이 된다.

```python
@app.patch("/photos/{id}")
async def udpate_partial(photo_update: PhotoUpdate):
```

위의 API에서는 PATCH 메서드를 사용하여 제목과 가격이 포함된 JSON 데이터를 전송한다,

```python
% http PATCH http://127.0.0.1:8000/photos/3 title="dog" price="300"
```

수신된 `PhotoUpdate` 개체를 출력하는 코드는 다음과 같다,

```python
@app.patch("/photos/{id}")
async def udpate_partial(photo_update: PhotoUpdate):
    print(photo_update.dict())
```

`filename`은 `None`이고 다른 값은 아래와 같이 채워져 있는 것을 볼 수 있다.

```python
{'title': 'dog', 'filename': None, 'price': 300}
```

또한 이를 `dict(exclude_unset=True)`를 사용하여 딕셔너리로 변환하면 아래와 같이 파일명을 제외한 모든 요소를 추출할 수 있다.

```python
@app.patch("/photos/{id}")
async def udpate_partial(photo_update: PhotoUpdate):
    print(photo_update.dict())
    data_to_update = photo_update.dict(exclude_unset=True)
    return data_to_update
```

이번에는 `date_to_update`를 클라이언트에 직접 반환하는 실험을 해보겠다.

결과는 다음과 같다.

```python
% http PATCH http://127.0.0.1:8000/photos/3 title="dog" price="300"
HTTP/1.1 200 OK
content-length: 27
content-type: application/json
date: Wed, 13 Sep 2023 07:50:08 GMT
server: uvicorn

{
    "price": 300,
    "title": "dog"
}
```

이렇게 하면 `price`와 `title`만 포함된 사전이 생성되는 것을 볼 수 있다. 이를 데이터베이스의 ORM 매퍼의 업데이트 함수에 전달하면 변경하고자 하는 요소만 업데이트할 수 있다.

각 REST 메서드 또는 DB의 각 연산에 대해 클래스를 생성하는 아이디어는 node.js에서의 프로그래밍 스타일과 달라서 Javascript에 익숙한 개발자에게는 조금 신선할 것이다.

이 시리즈에서 더 많은 내용을 기대해주세요...
